package ru.itis.covid.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.covid.clients.StopAndSearchClient;
import ru.itis.covid.entries.StopAndSearchStatistic;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class StopAndSearchServiceImpl implements StopAndSearchService {
    @Autowired
    private List<StopAndSearchClient> clients;
    @Override
    public List<StopAndSearchStatistic> getAll() {
        return clients.parallelStream()
                .flatMap(client -> client.getAll().stream())
                .collect(Collectors.toList());
    }
}
