package ru.itis.covid.entries;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class StopAndSearchStatistic {
    private String ageRange;
    private String ethnicity;
    private String objectOfSearch;
    private String legislation;
    private String from;
}
