package ru.itis.covid.entries;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class StopAndSearchRecord {
    @JsonProperty("age_range")
    private String ageRange;

    @JsonProperty("self_defined_ethnicity")
    private String ethnicity;

    @JsonProperty("object_of_search")
    private String objectOfSearch;

    @JsonProperty("legislation")
    private String legislation;
}
