package ru.itis.covid.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.covid.entries.StopAndSearchStatistic;
import ru.itis.covid.service.StopAndSearchService;

import java.util.List;

@RestController
@RequestMapping("/stop-and-search-management")
public class StopAndSearchController {

    @Autowired
    private StopAndSearchService service;

    @GetMapping("/records")
    public List<StopAndSearchStatistic> getAll() {
        return service.getAll();
    }
}
