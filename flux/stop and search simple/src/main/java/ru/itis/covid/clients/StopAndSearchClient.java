package ru.itis.covid.clients;

import ru.itis.covid.entries.StopAndSearchStatistic;

import java.util.List;

public interface StopAndSearchClient {
    List<StopAndSearchStatistic> getAll();
}
