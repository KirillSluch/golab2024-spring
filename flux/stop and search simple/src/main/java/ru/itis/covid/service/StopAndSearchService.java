package ru.itis.covid.service;

import ru.itis.covid.entries.StopAndSearchStatistic;

import java.util.List;

public interface StopAndSearchService {
    List<StopAndSearchStatistic> getAll();
}
