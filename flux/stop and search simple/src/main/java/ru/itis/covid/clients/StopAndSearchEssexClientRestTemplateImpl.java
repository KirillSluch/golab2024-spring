package ru.itis.covid.clients;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import ru.itis.covid.entries.StopAndSearchRecord;
import ru.itis.covid.entries.StopAndSearchStatistic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class StopAndSearchEssexClientRestTemplateImpl implements StopAndSearchClient {

    @Value("${uk.police.api.essex}")
    private String url;

    @Autowired
    private RestTemplate client;

    @Override
    public List<StopAndSearchStatistic> getAll() {
        StopAndSearchRecord[] response = client.getForEntity(url, StopAndSearchRecord[].class).getBody();
        List<StopAndSearchRecord> records = Arrays.asList(Objects.requireNonNull(response));
        return records.parallelStream()
                .map(record -> StopAndSearchStatistic.builder()
                        .ageRange(record.getAgeRange())
                        .objectOfSearch(record.getObjectOfSearch())
                        .ethnicity(record.getEthnicity())
                        .legislation(record.getLegislation())
                        .from("Essex")
                        .build())
                .collect(Collectors.toList());
    }
}
