package ru.itis.covid.clients;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import ru.itis.covid.entries.StopAndSearchRecord;
import ru.itis.covid.entries.StopAndSearchStatistic;

import java.util.Arrays;

@Component
public class StopAndSearchEssexClientRestWebClientImpl implements StopAndSearchClient {
    private WebClient client;

    public StopAndSearchEssexClientRestWebClientImpl(@Value("${uk.police.api.essex}") String url) {
        client = WebClient.builder()
                .exchangeStrategies(ExchangeStrategies.builder()
                        .codecs(clientCodecConfigurer -> clientCodecConfigurer.defaultCodecs().maxInMemorySize(100*1024*1024))
                        .build())
                .baseUrl(url)
                .build();
    }
    @Override
    public Flux<StopAndSearchStatistic> getAll() {
        return client.get()
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .flatMap(clientResponse -> clientResponse.bodyToMono(StopAndSearchRecord[].class))
                .flatMapIterable(Arrays::asList)
                .map(record -> StopAndSearchStatistic.builder()
                        .ageRange(record.getAgeRange())
                        .objectOfSearch(record.getObjectOfSearch())
                        .ethnicity(record.getEthnicity())
                        .legislation(record.getLegislation())
                        .from("Essex")
                        .build());
    }

//    @Value("${uk.police.api.essex}")
//    private String url;
//
//    @Autowired
//    private RestTemplate client;
//
//    @Override
//    public List<StopAndSearchStatistic> getAll() {
//        StopAndSearchRecord[] response = client.getForEntity(url, StopAndSearchRecord[].class).getBody();
//        List<StopAndSearchRecord> records = Arrays.asList(Objects.requireNonNull(response));
//        return records.parallelStream()
//                .map(record -> StopAndSearchStatistic.builder()
//                        .ageRange(record.getAgeRange())
//                        .objectOfSearch(record.getObjectOfSearch())
//                        .ethnicity(record.getEthnicity())
//                        .legislation(record.getLegislation())
//                        .from("Essex")
//                        .build())
//                .collect(Collectors.toList());
//    }
}
