package ru.itis.covid.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.itis.covid.entries.StopAndSearchStatistic;
import ru.itis.covid.service.StopAndSearchService;

import java.awt.*;
import java.util.List;

@RestController
@RequestMapping("/stop-and-search-management")
public class StopAndSearchController {

    @Autowired
    private StopAndSearchService service;

//    @GetMapping(value = "/records", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    @GetMapping(value = "/records")
    public Flux<StopAndSearchStatistic> getAll() {
        return service.getAll();
    }
}
