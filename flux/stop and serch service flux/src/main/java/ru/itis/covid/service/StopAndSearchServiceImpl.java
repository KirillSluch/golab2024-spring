package ru.itis.covid.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;
import ru.itis.covid.clients.StopAndSearchClient;
import ru.itis.covid.entries.StopAndSearchStatistic;

import java.util.List;
import java.util.concurrent.Flow;
import java.util.stream.Collectors;

@Service
public class StopAndSearchServiceImpl implements StopAndSearchService {
    @Autowired
    private List<StopAndSearchClient> clients;
    @Override
    public Flux<StopAndSearchStatistic> getAll() {
        List<Flux<StopAndSearchStatistic>> fluxes = clients.stream().map(this::getAll).toList();
        return Flux.merge(fluxes);
    }
//    @Override
//    public List<StopAndSearchStatistic> getAll() {
//        return clients.parallelStream()
//                .flatMap(client -> client.getAll().stream())
//                .collect(Collectors.toList());
//    }

    private Flux<StopAndSearchStatistic> getAll(StopAndSearchClient client) {
        return client.getAll().subscribeOn(Schedulers.boundedElastic());
    }
}
