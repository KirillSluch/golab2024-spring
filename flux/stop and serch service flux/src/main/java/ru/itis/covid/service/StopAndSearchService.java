package ru.itis.covid.service;

import reactor.core.publisher.Flux;
import ru.itis.covid.entries.StopAndSearchStatistic;

import java.util.List;

public interface StopAndSearchService {
    Flux<StopAndSearchStatistic> getAll();
}
