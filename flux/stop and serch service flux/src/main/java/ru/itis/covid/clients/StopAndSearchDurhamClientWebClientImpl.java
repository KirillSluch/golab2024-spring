package ru.itis.covid.clients;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import ru.itis.covid.entries.StopAndSearchRecord;
import ru.itis.covid.entries.StopAndSearchStatistic;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class StopAndSearchDurhamClientWebClientImpl implements StopAndSearchClient {
    @Value("${uk.police.api.durham}")
    private String url;

    private WebClient client;

    public StopAndSearchDurhamClientWebClientImpl(@Value("${uk.police.api.durham}") String url) {
        client = WebClient.builder()
                .exchangeStrategies(ExchangeStrategies.builder()
                        .codecs(clientCodecConfigurer -> clientCodecConfigurer.defaultCodecs().maxInMemorySize(100*1024*1024))
                        .build())
                .baseUrl(url)
                .build();
    }
    @Override
    public Flux<StopAndSearchStatistic> getAll() {
        return client.get()
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .flatMap(clientResponse -> clientResponse.bodyToMono(StopAndSearchRecord[].class))
                .flatMapIterable(Arrays::asList)
                .map(record -> StopAndSearchStatistic.builder()
                        .ageRange(record.getAgeRange())
                        .objectOfSearch(record.getObjectOfSearch())
                        .ethnicity(record.getEthnicity())
                        .legislation(record.getLegislation())
                        .from("Durham")
                        .build());
    }

//    @Override
//    public List<StopAndSearchStatistic> getAll() {
//        StopAndSearchRecord[] response = client.getForEntity(url, StopAndSearchRecord[].class).getBody();
//        List<StopAndSearchRecord> records = Arrays.asList(Objects.requireNonNull(response));
//        return records.parallelStream()
//                .map(record -> StopAndSearchStatistic.builder()
//                        .ageRange(record.getAgeRange())
//                        .objectOfSearch(record.getObjectOfSearch())
//                        .ethnicity(record.getEthnicity())
//                        .legislation(record.getLegislation())
//                        .from("Durham")
//                        .build())
//                .collect(Collectors.toList());
//    }
}
