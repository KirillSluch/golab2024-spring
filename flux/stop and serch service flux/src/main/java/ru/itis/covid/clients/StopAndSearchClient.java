package ru.itis.covid.clients;

import reactor.core.publisher.Flux;
import ru.itis.covid.entries.StopAndSearchStatistic;

import java.util.List;

public interface StopAndSearchClient {
    Flux<StopAndSearchStatistic> getAll();
}
