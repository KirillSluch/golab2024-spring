package core

import "go.mongodb.org/mongo-driver/bson/primitive"

type CV struct {
	ID                    primitive.ObjectID `bson:"_id,omitempty"`
	CandidateName         string             `bson:"candidateName,omitempty"`
	CandidateAge          uint               `bson:"candidateAge,omitempty"`
	Experience            uint               `bson:"experience,omitempty"`
	Skills                []string           `bson:"skills,omitempty"`
	ExperienceDescription string             `bson:"experienceDescription,omitempty"`
}
